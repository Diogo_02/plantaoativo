<?php

namespace App\Http\Controllers;

use \App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexView(){
        return view('plantao');
    }
     public function index()
    {
        $postagem = Post::all();
        //dd($postagem);
        return $postagem;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'author' => 'required|string',
            'tags' => 'required|string'
        ]);
        //dd($request->all());
        
        $postagem = Post::create([
            'title' => $request['title'],
            'content' => $request['content'],
            'author' => $request['author'],
            'tags' => $request['tags'],
        ]);
        return json_encode($postagem);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if (isset($post)) {
            return json_encode($post);            
        }
        return response('Postagem não encontrada', 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        //dd($post);
        if(isset($post)){
            return view('edit', compact('post'));
        }
        return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'author' => 'required|string',
            'tags' => 'required|string'
        ]);
        $post=Post::find($id);
        if (isset($post)) {   
            $post->title=$request->get('title');
            $post->content=$request->get('content');
            $post->author=$request->get('author');
            $post->tags=$request->get('tags');
            $post->save();
            return json_encode($post);
        }
        return response('Postagem não encontrada', 404);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if (isset($post)) {
            $post->delete();
            return response('OK', 200);
        }else{
            return response("Postagem não existe", 404);
        }     
    }

    public function busca($dados)
    {
        $post = Post::where('tags',$dados)->get(); 
        return ($post);           
    }

}
