<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class docApi extends Controller
{
    /**
     * /**
    * @OA\Info(
    *      version="1.0.0",
    *      title="Laravel Documentção",
    *      description="CRUD laravel 5.7 - Docummentação",
    *      @OA\Contact(
    *          email="diogosantos.inf@gmail.com"
    *      ),
    *      @OA\License(
    *          name="Apache 2.0",
    *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
    *      )
    * )
    *
    * @OA\Server(
    *      url=L5_SWAGGER_CONST_HOST,
    *      description="Demo API Server"
    * )

    *
    * @OA\Tag(
    *     name="user",
    *     description="Operações disponíveis para usuários."
    * )

    * @OA\Get(
    *       path="/",
    *       
    *       description="Home page",
    *     @OA\Response(response="default", description="Welcome page")
    * )
     */

}
