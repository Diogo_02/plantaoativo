<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id','title', 'content','author','tags'];
    
    public function getId(){return $this->id;}
    public function getTitle(){return $this->title;}
    public function getContent(){return $this->content;}
    public function getAuthor(){return $this->author;}
    public function getTags(){return $this->tags;}
}
