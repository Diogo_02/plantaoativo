swagger: '2.0'
info:
  description: Isto é uma simples API
  version: 1.0.0
  title: API Simples CRUD Laravel 5.7 
  # put the contact info for your development or API team
  contact:
    email: diogosantos.inf@ygmail.com

  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html

# tags are used for organizing operations
tags:
- name: Diogo Santos de Sousa
  description: Secured Admin-only calls
- name: developers
  description: Operações disponíveis para desenvolvedores regulares

paths:
    /api/post:
        get:
            tags:
            - developers
            summary: Todas as postagens
            operationId: mostrarPost
            description: |
                Inicialmente será apresentada todas as postagens já criadas.
            produces:
            - application/json
            parameters:
            - in: query
                name: searchString
                description: pass an optional search string for looking up inventory
                required: false
                type: string
            - in: query
                name: skip
                description: number of records to skip for pagination
                type: integer
                format: int32
                minimum: 0
            - in: query
                name: limit
                description: maximum number of records to return
                type: integer
                format: int32
                minimum: 0
                maximum: 50
            responses:
                200:
                description: search results matching criteria
                schema:
                    type: array
                    items:
                    $ref: '#/definitions/InventoryItem'
                400:
                description: bad input parameter
        post:
            tags:
            - admins
            summary:  adicionar uma nova postagem 
            operationId: addInventory
            description: Adds an item to the system
            consumes:
            - application/json
            produces:
            - application/json
            parameters:
            - in: body
                name: Post
                description: Inventory item to add
                schema:
                $ref: '#/definitions/InventoryItem'
            responses:
                201:
                description: item created
                400:
                description: invalid input, object invalid
                409:
                description: an existing item already exists

    /api/post/{post}
    /api/post/{post}
definitions:
  Post:
    type: object
    required:
    - id
    - title
    - content
    - author
    - tags
    properties:
      id:
        type: integer
        example: 1
      title:
        type: string
        example: Meu aniversário
      author:
        type: string
        example: Diogo Santos
      content:
        type: string
        example: Meu aniversário é dia 31/01
      tags:
        type: string
        example: Festa
        
      created_at:
        type: string
        format: date-time
        example: 2016-08-29T09:12:33.001Z

    update_at:
        type: string
        format: date-time
        example: 2016-08-29T09:12:33.001Z
     
# Added by API Auto Mocking Plugin
host: virtserver.swaggerhub.com
basePath : /diogo_02/plantaoAtivo/1.0.0
schemes:
 - https