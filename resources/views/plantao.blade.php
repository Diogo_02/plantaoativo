  
@extends('layouts.app')

@section('body')
<div class="jumbotron bg-light ">
    <button type="button" class="btn btn-primary" onclick="formPost()">Adicionar nova postagem</button>
    
    <div id="postagens" class="row">
        
    </div>
</div>

<!-- Modal Adicionar Post -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="api/post" method="post" id="formPost">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adicionar novo post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> 
                <div class="modal-body">
                    <input id="id" type="hidden" class="form-control" name="id" > 
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Título</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="content" class="col-md-4 col-form-label text-md-right">Conteúdo</label>
                        <div class="col-md-6">
                            <input id="content" type="text" class="form-control" name="content" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="author" class="col-md-4 col-form-label text-md-right">Autor</label>
                        <div class="col-md-6">
                            <input id="author" type="text" class="form-control" name="author" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tags" class="col-md-4 col-form-label text-md-right">Tags</label>
                        <div class="col-md-6">
                            <select id="tags" class="form-control" name="tags" required>
                                <option>Selecione uma Tag</option>
                                <option class="form-control" value="Esporte"> Esporte</option>
                                <option class="form-control" value="Política"> Política</option>
                                <option class="form-control" value="Lazer"> Lazer</option>
                                <option class="form-control" value="Saúde"> Saúde</option>
                                <option class="form-control" value="Gastronomia"> Gastronomia</option>
                                <option class="form-control" value="Curiosidades"> Curiosidades</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Salvar</button>
                    <button type="cancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@section('javascript')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    // MÉTODO QUE MOSTRA O MODAL(FORM)
    function formPost() {
        $('#Modal').modal('show');
    } 
    
    //CRIA UMA NOVA POSTAGEM E MOSTRA DIRETAMENTE
    function criarPost() {
        postagem = { 
            title: $("#title").val(), 
            author: $("#author").val(), 
            content: $("#content").val(), 
            tags: $("#tags").val() 
        };
        $.post("/api/post", postagem, function(data) {
            post = JSON.parse(data);
            card = montarCard(post);
            $('#postagens').append(card);  
            
        });
    }

    // MÉTODO QUE CRIA OS CARD
    function montarCard(p){
        var card='<div class="card m-2" id="'+p.id+'">'+
                    '<div class="card-body">'+
                        '<div class="alert alert-success"> '+
                            '<span class="badge badge-primary">'+p.tags+'</span>'+
                            '<h1>'+p.title+'</h1>'+
                            '<p class="font-weight-light">'+p.content+'</p>'+
                            
                        '</div>'+
                    '</div>'+
                    '<div class="card-footer bg-transparent border-success">'+
                        'Autor: '+p.author+
                        '<div class="btn-group dropup float-right m-1">'+
                            '<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '●●●</button>'+
                            '<div class="dropdown-menu p-2">'+
                                '<button class="btn btn-outline-primary btn-block m-1" onclick="editarForm('+p.id+')"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'+
                                    '<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                                    '</svg>'+
                                '</button>'+
                                '<button class="btn btn-outline-danger btn-block m-1  " onclick="apagarCard('+p.id+')">'+
                                    '<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'+
                                            '<path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>'+
                                    '</svg>'+
                                '</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                    return card;
    }

    function apagarCard(id){
        $.ajax({
            type: "DELETE",
            url: "/api/post/"+id,
            context: this,
            success: function () {
                var d = "#"+id;
                $(".card").filter($(d)).remove();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function editarForm(id) {
        $.getJSON('/api/post/'+id, function(data) { 
            $('#id').val(data.id);
            $('#title').val(data.title);
            $('#content').val(data.content);
            $('#author').val(data.author);
            $('#tags').val(data.tags);
            $('#Modal').modal('show');            
        });        
    }

    function salvarPost() {
        postagem = { 
            id: $("#id").val(), 
            title: $("#title").val(), 
            author: $("#author").val(), 
            content: $("#content").val(), 
            tags: $("#tags").val() 
        };
        $.ajax({
            type:"PUT",
            url: "/api/post/"+ postagem.id,
            data: postagem,
            context: this,
            success: function (data) {
                post = JSON.parse(data);
                var d = "#"+postagem.id;
                c = $(".card").filter($(d));
                if (c) {
                    c.html('<div class="card-body">'+
                        '<div class="alert alert-success"> '+
                            '<span class="badge badge-primary">'+post.tags+'</span>'+
                            '<h1>'+post.title+'</h1>'+
                            '<p class="font-weight-light">'+post.content+'</p>'+
                        '</div>'+
                    '</div>'+
                    '<div class="card-footer bg-transparent border-success">'+
                        'Autor: '+post.author+
                        '<div class="btn-group dropup float-right m-1">'+
                            '<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '●●●</button>'+
                            '<div class="dropdown-menu p-2">'+
                                '<button class="btn btn-outline-primary btn-block m-1" onclick="editarForm('+post.id+')"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'+
                                    '<path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                                    '</svg>'+
                                '</button>'+
                                '<button class="btn btn-outline-danger btn-block m-1  " onclick="apagarCard('+post.id+')">'+
                                    '<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'+
                                            '<path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>'+
                                    '</svg>'+
                                '</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>') ;
                }         
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    // MÉTODO MOSTRA OS CARD CRIADO CONFORME QUANT. DE DADOS 
    function mostrarPostagens() {
        $.getJSON('/api/post', function(data) {
            if (data.length>0) {
                for(i=0;i<data.length;i++) {
                card= montarCard(data[i]);
                $('#postagens').append(card);
                }
            } else {
                //$('#postagens').html("adicione uma nova postagem");
            }
            
        });
    }
    
    $('#buscarPost').submit(function(event){
        event.preventDefault();
        var dados = $("#busca").val();
        
        if (dados != '') {
            $.ajax({
            url: 'api/busca/'+dados,
            method: 'get',
            dataType: 'html',
            data: dados,
            success: function(data){
                if (data == '[]') {
                    console.log("dados nao encontrados");
                    alert("Não possui nenhum registro")
                } else {
                    post = JSON.parse(data);
                    console.log(post.length);
                    $("#postagens").empty();
                    for(var i = 0; i < post.length; i++) {
                        $("#postagens").append(montarCard(post[i]));
                    }
                    
                }

            }
            });
        }
        return false;
    });

            // $('#buscarPost').trigger('submit');


    //MÉTODO NÃO RECARREGA A PÁGINA
    $("#formPost").submit( function(event){ 
        event.preventDefault(); 
        if ($("#id").val() == '') {
            criarPost();
        } else {
            salvarPost();
        }
        
        $("#Modal").modal('hide');
    });

    $(function(){
        mostrarPostagens();   
    });
</script>
@endsection