<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


# API SIMPLES COM CRUD USANDO AJAX E JQUERY 

## INSTRUÇÕES PARA O FUNCIONAMENTO DO PROJETO:

composer install
cp .env.example .env
php artisan key:generate

 - Configurar .env
 - Adicionar Database e senha 

## DOCUMENTAÇÃO COM SWAGGER 

    Para acessar a documentação da API, coloque a url:loccalhost:8000/api/documentation

    
## AUTOR: Diogo Santos de Sousa
